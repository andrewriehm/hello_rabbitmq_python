#!/usr/bin/python
'''
This module has a docstring!
'''

# There's a lot of good stuff in this unittest module:
# https://docs.python.org/2/library/unittest.html
import unittest

class HelloWorldTests(unittest.TestCase):
    '''
    This is here to serve as a simple example.
    '''

    def test_foo(self):
        '''
        This should test some magical foo method.
        '''
        result = 2 + 2
        self.assertEqual(4, result)


# You can use this, but the nosetest runner has some advantages,
# ... so instead of $ python <this_file>
# ... try this:
# ... $ nosetests <this_file>

#if __name__ == '__main__':
#    unittest.main()
