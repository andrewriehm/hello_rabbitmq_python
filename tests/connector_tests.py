#!/usr/bin/python
'''
This module has a docstring!
'''

import unittest
import src.connector as connector

class ConnectorTests(unittest.TestCase):
    '''
    Test suite for the connector method
    '''

    def setUp(self):
        '''
        Tests the setup method.
        '''
        self.mq_server = "172.17.0.2"
        self.the_queue = "hello"


    def test_connect_should_complain(self):
        '''
        If you don't give it a server, it should complain.
        '''
        self.assertRaises(ValueError, connector.connect, "", "")

        self.assertRaises(ValueError, connector.connect, "172.17.0.2", "")


    def test_connect_should_connect(self):
        '''
        Makes sure the connect method saves the server
        '''
        connection = connector.connect(self.mq_server, self.the_queue)

        self.assertIsNotNone(connection)
