#!/usr/bin/python
'''
This module has a docstring!
'''

import unittest
import src.producer
import src.connector

class ProducerTests(unittest.TestCase):
    '''
    Test suite for the Producer class
    '''

    def setUp(self):
        '''
        Tests the setup method.
        '''
        self.mq_server = "172.17.0.2"
        self.the_queue = "hello"

    def test_should_have_produce(self):
        '''
        There should be a produce method, and it should... do... something...
        '''
        con = src.connector.connect(self.mq_server, self.the_queue)
        result = src.producer.produce(con)
        self.assertEqual(0, result)
