#!/usr/bin/python
'''
This module has a docstring!
'''

import connector

def callback(channel, method, properties, body):
    '''
    Callback when receiving a message from the queue.
    '''
    print " [X] Channel %r" % channel
    print " [X] Method %r" % method
    print " [X] Properties %r" % properties
    print " [X] Received %r" % body


def consume(connection):
    '''
    Call this to start consuming.
    '''

    if connection is None:
        raise ValueError("No connection detected - call 'connect' first!")

    channel = connection.channel()
    if channel is None:
        raise ValueError("No channel established - call 'connect' first!")

    channel.basic_consume(
        callback,
        queue='hello',
        no_ack=True
    )

    # This never stops...
    print " [*] Waiting for messages.  To exit, press CTRL-C"
    channel.start_consuming()


if __name__ == "__main__":
    consume(connector.connect("172.17.0.2", "hello"))
