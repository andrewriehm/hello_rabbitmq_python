#!/usr/bin/python
'''
When you need a connection, use this method
'''

import pika

def connect(server, the_queue):
    '''
    Call this to connect to the specified RabbitMQ server.
    '''
    if server == "":
        raise ValueError("No server given!")

    if the_queue == "":
        raise ValueError("No queue specified!")

    parms = pika.ConnectionParameters(server)
    connection = pika.BlockingConnection(parms)
    channel = connection.channel()
    channel.queue_declare(queue=the_queue)
    return connection
