#!/usr/bin/python
'''
This module has a docstring!
'''

import connector

def produce(the_connection):
    '''
    Call this to start producing.
    '''
    if the_connection is None:
        raise ValueError("No connection detected - call 'connect' first!")

    channel = the_connection.channel()
    if channel is None:
        raise ValueError("No channel established - call 'connect' first!")

    msg = "Hello, World!"
    channel.basic_publish(
        exchange='',
        routing_key='hello',
        body=msg)
    print " [X] Sent " + msg
    the_connection.close()

    return 0

if __name__ == "__main__":
    produce(connector.connect("172.17.0.2", "hello"))
