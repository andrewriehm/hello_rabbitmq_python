#!/bin/bash

# This deletes all the .pyc cruft that can accumulate.
# Because python is AWESOME.
find . -name "*.pyc" | xargs rm
