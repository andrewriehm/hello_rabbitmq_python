#!/bin/bash

# Stop the rabbitmq server
docker stop some-rabbit

# Remove the containers
docker rm $(docker ps -a -q)

# Remove all the images
# docker rmi $(docker images)
