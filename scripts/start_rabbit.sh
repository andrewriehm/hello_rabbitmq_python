#!/bin/bash

# Start up docker rabbitmq instance
# (from https://hub.docker.com/_/rabbitmq/)
docker run -d --hostname my-rabbit --name some-rabbit rabbitmq:3

# Show ip address of rabbitmq docker
ADDR=$(docker exec -it some-rabbit ip addr | grep "inet " | grep -v "127.0." | awk '{print $2}')
echo "Launched RabbitMQ docker image, IP: $ADDR"
